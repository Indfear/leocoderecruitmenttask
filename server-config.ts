export const serverConfig = {
    port: 3000,
    jwt: {
        secretKey: "MySuperSecretJWTKey",
        // Expiration time of the token in seconds
        expirationTime: 300,
    },
};
