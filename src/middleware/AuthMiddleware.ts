import * as express from "express";
import {injectable} from "inversify";
import passport from "passport";
import * as passportJWT from "passport-jwt";
import {VerifiedCallback} from "passport-jwt";
import {serverConfig} from "../../server-config";
import database from "../fakeDatabase/database";

@injectable()
export class AuthMiddleware {

    public ExtractJwt = passportJWT.ExtractJwt;
    public JwtStrategy = passportJWT.Strategy;
    public passport = passport;

    constructor() {
        this.config();
    }

    /**
     * Config JWT Strategy - the strategy will execute each time we authorize a route
     */
    public config(): void {
        const jwtOptions: any = {};
        jwtOptions.jwtFromRequest = this.ExtractJwt.fromAuthHeaderAsBearerToken();
        jwtOptions.secretOrKey = serverConfig.jwt.secretKey;
        jwtOptions.passReqToCallback = true;

        /** Create strategy */
        const strategy = new this.JwtStrategy(jwtOptions, async (req: any, jwtPayload: any, next: VerifiedCallback) => {

            /** Get token from request */
            const accessTokenFromRequest = this.getTokenFromHeader(req.headers.authorization);

            const user = database.users.find((u) => u.email === jwtPayload.email);

            if (user && typeof user !== "undefined") {
                next(null, user);
            } else {
                next(null, false);
            }
        });

        passport.use(strategy);
    }

    public authenticate = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        passport.authenticate("jwt", {session: false}, (err: any , user: any, info: any) => {
            if (err) {
                return next(err);
            }

            if (!user) {
                return res.status(401).json({
                    status: 401,
                    msg: "Unauthorized",
                    additionalInfo: info,
                });
            }
            res.locals.user = user;
            next();
        })(req, res, next);
    }

    /**
     * Get token from Authorization header
     * @param header - header from request
     */
    private getTokenFromHeader(header: string) {
        return header.replace("Bearer ", "");
    }
}
