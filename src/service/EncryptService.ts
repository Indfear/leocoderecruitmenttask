import {injectable} from "inversify";
import {IResponse, ISignIn, IUser} from "../interface/interfaces";
import * as fs from "fs";
import database from "../fakeDatabase/database";

@injectable()
export class EncryptService {
    public async getEncryptedFileAndUserPubKey(currentUser: IUser): Promise<IResponse> {
        const user = database.users.find((u) => u.email === currentUser.email);

        if (!user || typeof user === "undefined") {
            return {
                status: 500,
                data: null,
                msg: "Something went wrong. Please try again later",
            };
        }

        const encodedFile = await this.getEncryptedSamplePdf();

        let publicKey = null;
        if (!user.pubKey || typeof user.pubKey === "undefined") {
            publicKey = user.pubKey;
        }

        return {
            status: 200,
            data: {
                publicKey: publicKey,
                encryptedFile: encodedFile,
            },
            msg: "Success",
        };
    }

    private async getEncryptedSamplePdf(): Promise<string> {
        const file = await fs.readFileSync("./src/assets/sample.pdf");
        const encryptedFile = file.toString("base64");
        return encryptedFile;
    }
}
