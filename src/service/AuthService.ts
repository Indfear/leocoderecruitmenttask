import * as bcrypt from "bcrypt";
import {injectable} from "inversify";
import * as jwt from "jsonwebtoken";
import {serverConfig} from "../../server-config";
import database from "../fakeDatabase/database";
import {IResponse, ISignIn, IUser} from "../interface/interfaces";
// tslint:disable-next-line:no-var-requires
const generateRSAKeypair = require("generate-rsa-keypair");

@injectable()
export class AuthService {
    public async signIn(credentials: ISignIn): Promise<IResponse> {
        /** Check if user exists in the database */
        const user = database.users.find((u) => u.email === credentials.email);

        if (!user || typeof user === "undefined") {
            return {
                status: 404,
                data: null,
                msg: "User " + credentials.email + " does not exist",
            };
        }

        /** Check if given password matches the one in the database */
        const passwordIsCorrect = await bcrypt.compare(credentials.password, database.users[0].password);

        if (!passwordIsCorrect) {
            return {
                status: 401,
                data: null,
                msg: "Wrong password",
            };
        }

        /** Sign token */
        const token = jwt.sign({
            email: user.email,
        }, serverConfig.jwt.secretKey, { expiresIn: serverConfig.jwt.expirationTime})

        return {
            status: 200,
            data: {
                authToken: token,
            },
            msg: "Success",
        };
    }

    public async generateKeyPair(): Promise<IResponse> {
        const pair = generateRSAKeypair();

        return {
            status: 200,
            data: {
                privKey: pair.private,
                pubKey: pair.public,
            },
            msg: "Success",
        };
    }

    public async associateRSAToUser(loggedUser: IUser, privateKey: string, publicKey: string): Promise<void> {
        const user = database.users.find((u) => u.email === loggedUser.email);

        if (user && typeof user !== "undefined") {
            user.privKey = privateKey;
            user.pubKey = publicKey;
        }
    }
}
