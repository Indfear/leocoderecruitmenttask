const TYPES = {
    Controller: Symbol("Controller"),
    Service: Symbol("Service"),
};

export default TYPES;
