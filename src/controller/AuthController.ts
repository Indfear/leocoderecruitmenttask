import * as express from "express";
import {injectable} from "inversify";
import database from "../fakeDatabase/database";
import {IResponse} from "../interface/interfaces";
import Logger from "../logger/Logger";
import {AuthMiddleware} from "../middleware/AuthMiddleware";
import {AuthService} from "../service/AuthService";
import {IRegistrableController} from "./RegistrableController";

@injectable()
export class AuthController implements IRegistrableController {
    constructor(
        private authService: AuthService,
        private authMiddleware: AuthMiddleware,
    ) {
        this.authService = authService;
        this.authMiddleware = authMiddleware;
    }

    public register(app: express.Application): void {
        app.route("/api/sign-in")
            .post(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                try {
                    const response = await this.authService.signIn(req.body);

                    Logger.info("Successful call to /api/sign-in");
                    return res.status(200).json(response);
                } catch (e) {
                    Logger.error("Failed to sign in: " + e);
                    return res.status(500).json({
                        status: 500,
                        msg: "Something went wrong. Please try again later",
                        data: null,
                    });
                }
            });

        app.route("/api/generate-key-pair")
            .post(this.authMiddleware.authenticate,
                async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                    try {
                        const response = await this.authService.generateKeyPair();

                        const user = res.locals.user;
                        if (!user || typeof user === "undefined") {
                            Logger.error("Failed to load current logged user");
                            res.status(500).json({
                                status: 500,
                                msg: "Failed to load current logged user",
                                data: null,
                            });
                        }

                        await this.authService.associateRSAToUser(user, response.data.privKey, response.data.pubKey);

                        Logger.info("Successful call to /api/generate-key-pair");
                        return res.status(200).json(response);
                    } catch (e) {
                        Logger.error("Failed to generate key pair: " + e);
                        return res.status(500).json({
                            status: 500,
                            msg: "Something went wrong. Please try again later",
                            data: null,
                        });
                    }
                });
    }
}
