import * as express from "express";
import {injectable} from "inversify";
import Logger from "../logger/Logger";
import {AuthMiddleware} from "../middleware/AuthMiddleware";
import {EncryptService} from "../service/EncryptService";
import {IRegistrableController} from "./RegistrableController";

@injectable()
export class EncryptController implements IRegistrableController {
    constructor(
        private authMiddleware: AuthMiddleware,
        private encryptService: EncryptService,
    ) {
        this.authMiddleware = authMiddleware;
        this.encryptService = encryptService;
    }

    public register(app: express.Application): void {
        app.route("/api/encrypt")
            .post(this.authMiddleware.authenticate,
                async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                    try {
                        const user = res.locals.user;
                        if (!user || typeof user === "undefined") {
                            Logger.error("Failed to load current logged user");
                            res.status(500).json({
                                status: 500,
                                msg: "Failed to load current logged user",
                                data: null,
                            });
                        }

                        const response = await this.encryptService.getEncryptedFileAndUserPubKey(user);
                        Logger.info("Successful call to /api/encrypt");
                        return res.status(response.status).json(response);
                    } catch (e) {
                        Logger.error("Failed to sign in: " + e);
                        return res.status(500).json({
                            status: 500,
                            msg: "Something went wrong. Please try again later",
                            data: null,
                        });
                    }
                });
    }
}
