export interface IResponse {
    status: number;
    msg: string;
    data: any;
}

export interface IUser {
    email: string;
    password: string;
    privKey?: string;
    pubKey?: string;
}

export interface ISignIn {
    email: string;
    password: string;
}