import * as bcrypt from "bcrypt";
import bodyParser = require("body-parser");
import * as express from "express";
import "reflect-metadata";
import {IRegistrableController} from "./controller/RegistrableController";
import database from "./fakeDatabase/database";
import container from "./inversify.config";
import TYPES from "./types";

class App {
    public app: express.Application;

    constructor() {
        this.app = express.default();
        this.config();
    }

    private async config(): Promise<void> {
        this.app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            next();
        });

        this.app.use(bodyParser.json());
        // Form data can be stored in many ways so we use this to make sure that data will be in the request body
        this.app.use(bodyParser.urlencoded({
            extended: true,
        }));

        // allow cross origin requests
        this.app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            next();
        });

        // Grab the Controllers from IoC container and register all the endpoints
        const controllers: IRegistrableController[] = container.getAll<IRegistrableController>(TYPES.Controller);
        controllers.forEach((controller) => controller.register(this.app));

        // insert users to fake database
        const encryptedPassword1 = await bcrypt.hash("1234", 10);
        const encryptedPassword2 = await bcrypt.hash("12345", 10);
        database.users.push({
            email: "example@mail.com",
            password: encryptedPassword1,
        });

        database.users.push({
            email: "example2@mail.com",
            password: encryptedPassword2,
        });
    }
}

export default new App().app;
