import * as winston from "winston";

class Logger {
    public logger: winston.Logger;

    constructor() {
        this.logger = winston.createLogger({
            format: winston.format.json(),
            level: "info",
            transports: [
                new winston.transports.File({ filename: "error.log", level: "error"}),
                new winston.transports.File({ filename: "combined.log" }),
            ],
        });

        this.logger.add(new winston.transports.Console({
            format: winston.format.simple(),
        }));
    }
}

export default new Logger().logger;
