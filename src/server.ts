import {serverConfig} from "../server-config";
import app from "./app";
import Logger from "./logger/Logger";

app.listen(serverConfig.port, () => {
    Logger.info("Application listening on port " + serverConfig.port + "!");
});
