import { Container} from "inversify";
import "reflect-metadata";
import { AuthService } from "./service/AuthService";
import TYPES from "./types";

import {AuthController} from "./controller/AuthController";
import { IRegistrableController} from "./controller/RegistrableController";
import {AuthMiddleware} from "./middleware/AuthMiddleware";
import {EncryptController} from "./controller/EncryptController";
import {EncryptService} from "./service/EncryptService";

const container = new Container();

/** Controller bindings */
container.bind<IRegistrableController>(TYPES.Controller).to(AuthController);
container.bind<IRegistrableController>(TYPES.Controller).to(EncryptController);

/** Services bindings */
container.bind<AuthService>(AuthService).to(AuthService);
container.bind<EncryptService>(EncryptService).to(EncryptService);

/** Middlewares bindings */
container.bind<AuthMiddleware>(AuthMiddleware).to(AuthMiddleware);

export default container;
