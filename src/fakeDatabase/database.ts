import {IUser} from "../interface/interfaces";

const database = {
    users: [] as IUser[],
};

export default database;
